﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;

namespace WpfApplication2
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ImgUsuario_click(object sender, MouseEventArgs e)
        {
            UsuarioGeneral usg = new UsuarioGeneral();
            usg.Show();
        }

        private void btnUsuario_Click(object sender, RoutedEventArgs e)
        {
            UsuarioGeneral ug = new UsuarioGeneral();
            ug.Show();
        }
    }
}
